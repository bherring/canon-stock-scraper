from lxml import html
import requests
import smtplib
from smtplib import SMTPException
import sys, getopt

### Canon Stock Scraper
## Description: A Python script for scraping the Canon store to determine if an item is in stock.
## Version: 	0.0.2
## Developers:
#	Brad Herring - brad@bherville.com


def get_arguments(argv):
    arg_table 		= {} # The returned Dictionary containin the arguments
    item 			= '' # The item to check
    mailing_list 	= '' # Mailing List
    smtp_server 	= '' # SMTP Server
    
    # Usage help string
    usage_string 	= """canon_stock_scraper.py 
    -i 
    	item url name, i.e. the "ef-24-70mm-f-28l-ii-usm-refurbished" in the URL "http://shop.usa.canon.com/shop/en/catalog/ef-24-70mm-f-28l-ii-usm-refurbished".
    --always_send
    	always send a notification whether the item is in stock or not.
    -m
    	a comma seperated list of email addresses to notify.
    -s
    	the hostname of your SMTP server (smtp.gmail.com:587)
    --smtp_start_tls
    	use Start TLS with the SMTP server (use this for Gmail)
    --smtp_username
    	username to use to send email (your Gmail email address when using Gmail)
    --smtp_password
    	password to use to send email (your Gmail account password when using Gmail)
"""

	## Process command line arguments
    try:
        opts, args = getopt.getopt(argv,"hi:m:s:",["item=","always_send", "mailing_list=", "smtp_server=", "smtp_start_tls", "smtp_username=", "smtp_password="])
    except getopt.GetoptError:
        print usage_string
        sys.exit(2)
    for opt, arg in opts:
    	# Print Usage info
        if opt == '-h':
            print usage_string
            sys.exit()
        # Item url name, i.e. the "ef-24-70mm-f-28l-ii-usm-refurbished" in the URL 
        #	"http://shop.usa.canon.com/shop/en/catalog/ef-24-70mm-f-28l-ii-usm-refurbished".
        elif opt in ("-i", "--item"):
            item = arg
            arg_table['item'] = item
        # always send a notification whether the item is in stock or not. True or False
        elif opt in ("--always_send"):
            arg_table['always_send'] = True
        # a comma seperated list of email addresses to notify.
        elif opt in ("-m", "--mailing_list"):
            mailing_list = arg
            arg_table['mailing_list'] = mailing_list
            
        ## SMTP Server Argumnets
        # the hostname of your SMTP server (smtp.gmail.com:587)
        elif opt in ("-s", "--smtp_server"):
            smtp_server = arg
            arg_table['smtp_server'] = smtp_server            
        # use Start TLS with the SMTP server (use this for Gmail)
        elif opt in ("--smtp_start_tls"):
            arg_table['smtp_start_tls'] = True
        # username to use to send email (your Gmail email address when using Gmail)
        elif opt in ("--smtp_username"):
            smtp_username = arg
            arg_table['smtp_username'] = smtp_username
        # password to use to send email (your Gmail account password when using Gmail)
        elif opt in ("--smtp_password"):
            smtp_password = arg
            arg_table['smtp_password'] = smtp_password
          
    # Set up always_send false default.  
    if 'always_send' not in arg_table:
        arg_table['always_send'] = False
        	
	## SMTP server
	# Set the default SMTP server if not passed	
    if 'smtp_server' not in arg_table:
        arg_table['smtp_server'] = 'localhost'		
	
	# Set the default SMTP start tls if not passed
    if 'smtp_start_tls' not in arg_table:
        arg_table['smtp_start_tls'] = False		
	
	# Set the default SMTP username if not passed
    if 'smtp_username' not in arg_table:
        arg_table['smtp_username'] = False		
	
	# Set the default SMTP password if not passed
    if 'smtp_password' not in arg_table:
        arg_table['smtp_password'] = False
	
	# Set the default SMTP mailing_list if not passed
    if 'mailing_list' not in arg_table:
        arg_table['mailing_list'] = False
		
	
	# Throw error and show usage string of item was not passed.
    if 'item' not in arg_table:
        print "Error: You must -i/--item\n"
        print usage_string
        sys.exit(2)
		          
    return arg_table

if __name__ == "__main__":
    # Canon Store Base URL
    CANON_STORE_BASE_URL = 'http://shop.usa.canon.com/shop/en/catalog'

	# Process the command line arguments
    arg_table = get_arguments(sys.argv[1:])

	# Retreive the Canon store page for the item
    page = requests.get('{0}/{1}'.format(CANON_STORE_BASE_URL, arg_table['item']))
    # Get the XPath tree from the returned HTML
    tree = html.fromstring(page.content)
	# Retreive the title of the item page (this is basically the name of the item)
    page_title = tree.xpath('//title/text()')[0].replace(' | Canon Online Store', '')

	## Determine if the item is in stock or not
    in_stock_html = tree.xpath('//a[@id="add2CartBtn"][contains(@onclick, "return false;")]')

    print "Item:        \t", page_title
	# If the anchor add2CartBtn has an onclick attribute that is set to return false; than it is out of stock
    if len(in_stock_html) > 0:
        print "Stock Status:\tOut of Stock"
        in_stock = False
        in_stock_string = "out of stock"
	# If the anchor add2CartBtn does not have an onclick attribute that is set to return false; than it is in stock
    else:
        print "Stock Status:\tIn Stock"
        in_stock = True
        in_stock_string = "in stock"

	# If a mailing list was passed in and the item is either in stock or always_send is enabled,
	#	process and send the notification.
    if arg_table['mailing_list'] != False and (in_stock or arg_table['always_send']):
        sender = 'no_reply@canon_stock_scrapper.mx02.bherville.com'
        receivers = arg_table['mailing_list'].split(',')

        message = """From: %s
To: %s
Subject: Canon Stock Scrapper - %s - %s

Hello,
This is a notification that the item '%s' is %s.

Canon Store Link: %s
"""

        print "\n"
        print "Sending Notification"
        print "SMTP Server:", arg_table['smtp_server']
        print "Mailing List:", arg_table['mailing_list']
        try:
            # Setup the SMTP server
            smtpObj = smtplib.SMTP(arg_table['smtp_server'])
            if arg_table['smtp_start_tls']:
                smtpObj.starttls()
            if arg_table['smtp_username'] != False and arg_table['smtp_password'] != False:
                smtpObj.login(arg_table['smtp_username'],arg_table['smtp_password'])
            
            smtpObj.sendmail(sender, receivers, (message % (sender, arg_table['mailing_list'], page_title, in_stock_string, page_title, in_stock_string, '{0}/{0}'.format(CANON_STORE_BASE_URL, arg_table['item']))))
            print "Successfully sent email"
        except SMTPException as e:
            print "Error: unable to send email"
            print "\tException: ", e.message, e.args
