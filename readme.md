Canon Stock Scraper
================================
A Python script for scraping the Canon store to determine if an item is in stock.

#### Table of Contents
* [Requirements](#markdown-header-requirements)

    - [Required Software](#markdown-header-required-software)

    - [Tested Operating Systems](#markdown-header-tested-operating-systems)

* [Installation](#markdown-header-installation)

    - [Install lxml](#markdown-header-install-lxml)

    - [Install requests](#markdown-header-install-requests)

    - [Clone Repository](#markdown-header-clone-repository)

* [Usage](#markdown-header-usage)

    - [Getting Help](#markdown-header-getting-help)

    - [Running the Script](#markdown-header-running-the-script)

    - [Example Cron Job](#markdown-header-example-cron-job)

- - -

Requirements
================================
### Required Software
* Python 2.6.x (could work on older versions)
* Python Modules
	* lxml
	* requests

### Tested Operating Systems
* CentOS 6.7
* Fedora 21

Installation
================================
### Install lxml
RedHat/CentOS/Fedora via YUM
```
#!shell

yum install python-lxml
```

Ubuntu via APT
```
#!shell

apt-get install python-lxml
```

Via PIP
```
#!shell

pip install lxml
```

### Install requests
RedHat/CentOS/Fedora via YUM
```
#!shell

yum install python-requests
```

Ubuntu via APT
```
#!shell

apt-get install python-requests
```

Via PIP
```
#!shell

pip install requests
```

### Clone Repository
```
#!shell

git clone http://bitbucket.org/bherring/canon-stock-scraper
```

Usage
================================
### Getting Help
```
#!shell

$ python canon_stock_scraper.py -h
canon_stock_scraper.py 
    -i 
    	item url name, i.e. the "ef-24-70mm-f-28l-ii-usm-refurbished" in the URL "http://shop.usa.canon.com/shop/en/catalog/ef-24-70mm-f-28l-ii-usm-refurbished".
    --always_send
    	always send a notification whether the item is in stock or not.
    -m
    	a comma seperated list of email addresses to notify.
    -s
    	the hostname of your SMTP server (smtp.gmail.com:587)
    --smtp_start_tls
    	use Start TLS with the SMTP server (use this for Gmail)
    --smtp_username
    	username to use to send email (your Gmail email address when using Gmail)
    --smtp_password
    	password to use to send email (your Gmail account password when using Gmail)
```

### Running the Script
```
#!shell

$ python canon_stock_scraper.py -i ef-s-24mm-f-28-stm-refurbished -m 'test@example.com' -s smtp.gmail.com:587 --smtp_start_tls --smtp_username me@gmail.com --smtp_password GMAIL_PASSWORD
Item:        	Refurbished EF-S 24mm f/2.8 STM Wide Angle lens
Stock Status:	In Stock


Sending Notification
SMTP Server: smtp.gmail.com:587
Mailing List: test@example.com
Successfully sent email
```

### Example Cron Job
This will run the script every 30 minutes of every hour logging the current date and time and the script output to the /var/log/canon-stock-scraper.log log file.
```
#!shell

*/30 * * * * date >> /var/log/canon-stock-scraper.log && python /scripts/canon-stock-scraper/canon_stock_scraper.py -i ef-s-24mm-f-28-stm-refurbished -m 'test@example.com' -s smtp.gmail.com:587 --smtp_start_tls --smtp_username me@gmail.com --smtp_password GMAIL_PASSWORD >> /var/log/canon-stock-scraper.log 2>&1
```
